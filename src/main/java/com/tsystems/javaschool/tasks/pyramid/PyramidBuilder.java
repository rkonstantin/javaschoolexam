package com.tsystems.javaschool.tasks.pyramid;

import java.util.Collections;
import java.util.Iterator;
import java.util.List;

public class PyramidBuilder {

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    public int[][] buildPyramid(List<Integer> inputNumbers) {
        if (inputNumbers.contains(null)) {
            throw new CannotBuildPyramidException();
        }

        int size = inputNumbers.size();
        long radicand = 8 * size + 1; // size of inputNumbers should be a triangular number
        int sqrtFloor = (int) Math.sqrt(radicand);
        if (sqrtFloor * sqrtFloor != radicand) {
            throw new CannotBuildPyramidException();
        }

        int rows = (sqrtFloor - 1) / 2;
        int columns = 2 * rows - 1;
        int[][] pyramid = new int[rows][columns];

        Collections.sort(inputNumbers);

        Iterator<Integer> inputIterator = inputNumbers.iterator();
        for (int i = 0; i < rows; i++) {
            for (int j = rows - 1 - i; j < rows + i; j = j + 2) {
                pyramid[i][j] = inputIterator.next();
            }
        }

        return pyramid;
    }
}
