package com.tsystems.javaschool.tasks.calculator;

import java.text.DecimalFormat;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.NoSuchElementException;

public class Calculator {

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */
    public String evaluate(String statement)
    {
        if (statement == null || !statement.matches("[0-9()+\\-*/.]+")) return null;

        char[] chars = statement.toCharArray();

        LinkedList<Double> numbers = new LinkedList<>();
        LinkedList<Character> operations = new LinkedList<>();

        // statement can't start with such characters
        if (chars[0] == ')' || chars[0] == '*' || chars[0] == '/' || chars[0] == '.') {
            return null;
        }

        // consider '-' at the start as "(-1)*"
        if (chars[0] == '-') {
            numbers.add(-1.0);
            operations.add('*');
            chars = Arrays.copyOfRange(chars, 1, chars.length);
        }

        boolean numberExpected = true; // or left parenthesis
        boolean parenthesesOpen = false;

        for (int i = 0; i < chars.length; i++)
        {
            if (numberExpected) {
                if (isDigit(chars[i]) || (chars[i] == '-' && chars[i - 1] == '(')) {
                    StringBuilder sb = new StringBuilder(String.valueOf(chars[i]));
                    boolean pointWritten = false;
                    while (++i < chars.length) {
                        if (isDigit(chars[i])) {
                            sb.append(chars[i]);
                        } else if (!pointWritten && chars[i] == '.') {
                            sb.append(chars[i]);
                            pointWritten = true;
                            if (!isDigit(chars[i + 1])) return null;
                        } else break;
                    }
                    i--;
                    numbers.add(Double.parseDouble(sb.toString()));
                    numberExpected = false;
                } else if (chars[i] == '(') {
                    operations.add(chars[i]);
                    parenthesesOpen = true;
                }
                else return null;
            }

            else {
                if (chars[i] == '(') {
                    operations.add(chars[i]);
                    numberExpected = true;
                }

                else if (chars[i] == ')')
                {
                    if (parenthesesOpen) {
                        while (operations.getLast() != '(') {
                            numbers.add(performOp(operations.removeLast(), numbers.removeLast(), numbers.removeLast()));
                        }
                        operations.removeLast();
                    } else return null;
                }

                else if (chars[i] == '+' || chars[i] == '-' || chars[i] == '*' || chars[i] == '/')
                {
                    while (!operations.isEmpty() && hasPrecedence(chars[i], operations.getLast())) {
                        numbers.add(performOp(operations.removeLast(), numbers.removeLast(), numbers.removeLast()));
                    }
                    operations.add(chars[i]);
                    numberExpected = true;
                }
            }
        }

        while (!operations.isEmpty()) {
            try {
                numbers.add(performOp(operations.removeLast(), numbers.removeLast(), numbers.removeLast()));
            } catch (NoSuchElementException e) {
                return null;
            }
        }

        return Double.isFinite(numbers.getLast()) ?
                new DecimalFormat("#.####").format(numbers.removeLast()).replace(',','.') :
                null;
    }

    // returns true if op2 has higher or same precedence of op1
    private static boolean hasPrecedence(char op1, char op2)
    {
        if (op2 == '(' || op2 == ')') {
            return false;
        }
        if ((op1 == '*' || op1 == '/') && (op2 == '+' || op2 == '-')) {
            return false;
        }
        return true;
    }

    private static Double performOp(char op, Double b, Double a)
    {
        switch (op)
        {
            case '+':
                return a + b;
            case '-':
                return a - b;
            case '*':
                return a * b;
            case '/':
                return a / b;
        }
        return null;
    }

    private static boolean isDigit(char c) {
        return c >= '0' && c <= '9';
    }
}
