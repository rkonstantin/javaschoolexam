package com.tsystems.javaschool.tasks.subsequence;

import java.util.List;

public class Subsequence {

    /**
     * Checks if it is possible to get a sequence which is equal to the first
     * one by removing some elements from the second one.
     *
     * @param x first sequence
     * @param y second sequence
     * @return <code>true</code> if possible, otherwise <code>false</code>
     */
    @SuppressWarnings("rawtypes")
    public boolean find(List x, List y) {
        if (x == null || y == null) {
            throw new IllegalArgumentException();
        }
        boolean result = true;
        int newStart = 0;
        for (Object a : x) {
            result = false;
            for (int i = newStart; i < y.size(); i++) {
                if (a.equals(y.get(i))) {
                    result = true;
                    break;
                } else {
                    newStart++;
                }
            }
        }
        return result;
    }
}
